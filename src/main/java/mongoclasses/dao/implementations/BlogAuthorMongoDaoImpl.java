package mongoclasses.dao.implementations;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

import mongoclasses.beans.BlogAuthor;
import mongoclasses.dao.interfaces.BlogAuthorDao;
import mongoclasses.dao.interfaces.MorphiaDao;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class BlogAuthorMongoDaoImpl extends MorphiaDao implements BlogAuthorDao {

	public BlogAuthorMongoDaoImpl(String collection) {
		super(collection);
	}

	public BlogAuthor findAuthorbyId(String userName) {
		if(StringUtils.isNotEmpty(userName)){

	Query<BlogAuthor> query = datastore.createQuery(BlogAuthor.class).field("_id").equalIgnoreCase(userName);return query.get();
}else return null;}

	public List<BlogAuthor> findAuthorsbyUsername(String userName) {
		Query<BlogAuthor> query = datastore.createQuery(BlogAuthor.class).field("_id").containsIgnoreCase(userName);
		return query.asList();
	}
	
	
	public List<BlogAuthor> findAll(){
		Query<BlogAuthor> query = datastore.find(BlogAuthor.class);
		return query.asList();
		
	}

	public boolean createAuthor(BlogAuthor blogAuthor) {
		
		if(blogAuthor==null)return false;

		if (findAuthorbyId(blogAuthor.getUserName()) == null) {
			String passwordHash = makePasswordHash(blogAuthor.getPasswd(), Integer.toString(random.nextInt()));
			blogAuthor.setPasswd(passwordHash);
			Key<BlogAuthor> userKey = datastore.save(blogAuthor);
			log.debug(userKey.toString() + " " + userKey.getCollection());
			return true;
		} else
			return false;
	}

	public boolean updateAuthor(BlogAuthor blogAuthor) {
		if (findAuthorbyId(blogAuthor.getUserName()) != null) {
			Key<BlogAuthor> userKey = datastore.save(blogAuthor);
			log.debug(userKey.toString() + " " + userKey.getCollection());
			return true;
		} else
			return false;
	}
	
	public boolean updatePassword(BlogAuthor blogAuthor){
		String passwordHash = makePasswordHash(blogAuthor.getPasswd(), Integer.toString(random.nextInt()));
		blogAuthor.setPasswd(passwordHash);
		if (findAuthorbyId(blogAuthor.getUserName()) != null) {
			Key<BlogAuthor> userKey = datastore.save(blogAuthor);
			log.debug(userKey.toString() + " " + userKey.getCollection());
			return true;
		} else
			return false;
	}

	public boolean deleteAuthor(String userName) {
		if (datastore.delete(BlogAuthor.class, userName) != null)
			return true;
		else
			return false;
	}

	private String makePasswordHash(String passwd, String salt) {
		try {
			String saltedAndHashed = passwd + "," + salt;
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(saltedAndHashed.getBytes());
			BASE64Encoder encoder = new BASE64Encoder();
			byte hashedBytes[] = (new String(digest.digest(), "UTF-8")).getBytes();
			return encoder.encode(hashedBytes) + "," + salt;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5 is not available", e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
		}
	}

	public boolean checkuserCredentials(String userName, String passwd) {
		boolean validUser = false;
		BlogAuthor userToCheck = findAuthorbyId(userName);
		if (userToCheck != null) {
			String hashedAndSalted = userToCheck.getPasswd();
			String salt = hashedAndSalted.split(",")[1];
			passwd = makePasswordHash(passwd, salt);
			if (userToCheck.getPasswd().equals(passwd)) {
				validUser = true;
			}
		}
		return validUser;
	}
}
