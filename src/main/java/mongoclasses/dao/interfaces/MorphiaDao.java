package mongoclasses.dao.interfaces;

import java.security.SecureRandom;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import mongoclasses.dao.localdatetimeconverter.LocalDateTimeConverter;

public abstract class MorphiaDao {
	
	protected Random random = new SecureRandom();
	protected Logger log = LogManager.getLogger();
	final Morphia morphia;
	protected final Datastore datastore;

	
	protected MorphiaDao(String collection) {
		morphia = new Morphia();
		morphia.getMapper().getConverters().addConverter(new LocalDateTimeConverter());
		datastore = morphia.createDatastore(new MongoClient(), collection);
		datastore.ensureIndexes();
	}

}
