package mongoclasses.dao.interfaces;

import java.util.List;

import mongoclasses.beans.BlogAuthor;



public interface BlogAuthorDao {
	BlogAuthor findAuthorbyId(String userName);
	List<BlogAuthor> findAuthorsbyUsername(String userName);
	List<BlogAuthor> findAll();

	boolean checkuserCredentials(String userName, String passwd);
	boolean createAuthor(BlogAuthor blogAuthor);
	boolean updateAuthor(BlogAuthor blogAuthor);
	boolean deleteAuthor(String userName);
	boolean updatePassword(BlogAuthor blogAuthor);
	
}
