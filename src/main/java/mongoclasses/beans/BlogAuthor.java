package mongoclasses.beans;

import java.time.LocalDateTime;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
@Entity(value="author", noClassnameStored=true )
public class BlogAuthor {
	
	
	@Id
	@Reference
	private String userName;
	private String passwd;
	private LocalDateTime timeStamp;
	@Embedded
	private UserDetails userDetails;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	
	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	public UserDetails getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
		
}
